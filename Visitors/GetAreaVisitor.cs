﻿using MRX_8.Shapes;

namespace MRX_8.Visitors
{
    public class GetAreaVisitor : IVisitor
    {
        public string OperationName
            => "GetArea";
        
        public object Visit(Rectangle shape)
        {
            double height = Vec2.GetDistance(shape.Vertices[1], shape.Vertices[0]);
            double width = Vec2.GetDistance(shape.Vertices[2], shape.Vertices[1]);

            return (height * width);
        }

        public object Visit(Triangle shape)
        {
            // S = 1/2 * ((х1 - х3)(у2 - у3) - (х2 - х3)(у1 - у3))
            Vec2[] v = shape.Vertices;
            double interjacentResult = 
                (v[0].X - v[2].X) * (v[1].Y - v[2].Y) - (v[1].X - v[2].X) * (v[0].Y - v[2].Y)
                ;

            return interjacentResult / 2;
        }

        public object Visit(Octagon shape)
        {
            // Non-trivial
            return double.PositiveInfinity;
        }
    }
}
