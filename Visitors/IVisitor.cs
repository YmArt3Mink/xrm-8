﻿using MRX_8.Shapes;

namespace MRX_8.Visitors
{
    public interface IVisitor
    {
        string OperationName { get; }
        object Visit(Rectangle shape);
        object Visit(Triangle shape);
        object Visit(Octagon shape);
    }
}
