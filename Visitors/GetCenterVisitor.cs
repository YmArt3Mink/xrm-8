﻿using MRX_8.Shapes;

namespace MRX_8.Visitors
{
    public class GetCenterVisitor : IVisitor
    {
        public string OperationName
            => "GetCenter";

        public object Visit(Rectangle shape)
        {
            return
                new Vec2
                {
                    X = (shape.Vertices[2].X - shape.Vertices[1].X) / 2,
                    Y = (shape.Vertices[1].Y - shape.Vertices[0].Y) / 2
                };
        }

        public object Visit(Triangle shape)
        {
            // _blank
            return
                new Vec2
                {
                    X = 0,
                    Y = 0
                };
        }

        public object Visit(Octagon shape)
        {
            // _blank
            return
                new Vec2
                {
                    X = 0,
                    Y = 0
                };
        }
    }
}
