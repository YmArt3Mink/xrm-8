﻿using System;
using System.Linq;
using MRX_8.Shapes;

namespace MRX_8.Visitors
{
    public class DrawVisitor : IVisitor
    {
        public string OperationName
            => "Draw";

        private void Draw(ShapeBase shape)
        {
            int idx = 0;
            shape.Vertices
                .ToList()
                .ForEach(vertex =>
                    Console.Write($"{idx++}: {vertex}; ")
                );
            Console.WriteLine();
        }

        public object Visit(Rectangle shape)
        {
            Draw(shape);
            return new object();
        }

        public object Visit(Triangle shape)
        {
            Draw(shape);
            return new object();
        }

        public object Visit(Octagon shape)
        {
            Draw(shape);
            return new object();
        }
    }
}
