﻿using System;

namespace MRX_8.Shapes
{
    public struct Vec2
    {
        public double X, Y;

        public override string ToString()
        {
            return $"({X};{Y})";
        }

        public static double GetDistance(Vec2 lhs, Vec2 rhs)
        {
            var substrVec = lhs - rhs;
            substrVec = new Vec2(substrVec.X * substrVec.X, substrVec.Y + substrVec.Y);
            return 
                Math.Sqrt(substrVec.X + substrVec.Y)
                ;
        }

        public static Vec2 operator- (Vec2 lhs, Vec2 rhs)
        {
            return
                new Vec2(lhs.X - rhs.X, lhs.Y - rhs.Y)
                ;
        }

        public Vec2(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
