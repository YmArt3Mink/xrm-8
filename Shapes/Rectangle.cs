﻿using System;
using MRX_8.Visitors;

namespace MRX_8.Shapes
{
    public class Rectangle : ShapeBase
    {
        public override string Name
            => "Rectangle";

        private const int _verticesCount = 4;

        public override Vec2[] Vertices { get; }

        public override void Accept(IVisitor vis)
        {
            vis.Visit(this);
        }

        public Rectangle(Vec2[] vertices)
        {
            if (vertices.Length != _verticesCount)
            {
                throw new FormatException($"Vertices array for {Name} is to hast {_verticesCount} elements");
            }

            Vertices = vertices;
        }
    }
}
