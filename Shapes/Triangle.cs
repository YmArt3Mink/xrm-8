﻿using System;
using MRX_8.Visitors;

namespace MRX_8.Shapes
{
    public class Triangle : ShapeBase
    {
        public override string Name
            => "Triangle";

        private const int _verticesCount = 3;

        public override Vec2[] Vertices { get; }

        public override void Accept(IVisitor vis)
        {
            vis.Visit(this);
        }

        public Triangle(Vec2[] vertices)
        {
            if (vertices.Length != _verticesCount)
            {
                throw new FormatException($"Vertices array for {Name} is to hast {_verticesCount} elements");
            }

            Vertices = vertices;
        }
    }
}
