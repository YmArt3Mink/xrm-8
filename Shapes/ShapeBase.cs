﻿using MRX_8.Visitors;

namespace MRX_8.Shapes
{
    public abstract class ShapeBase
    {
        public abstract string Name { get; }

        public abstract Vec2[] Vertices { get; }

        public abstract void Accept(IVisitor vis);
    }
}
